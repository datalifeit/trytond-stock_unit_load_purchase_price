# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.modules.product import price_digits
from trytond.model import fields
from trytond.pyson import Eval, Bool, Not
from decimal import Decimal


class UnitLoad(metaclass=PoolMeta):
    __name__ = 'stock.unit_load'

    purchase_price = fields.Numeric('Cost price', digits=price_digits,
        states={
            'readonly': Not(Bool(Eval('purchase_price_edit')))
        },
        depends=['purchase_price_edit'])
    purchase_price_edit = fields.Function(
        fields.Boolean('Edit Purchase price'),
        'on_change_with_purchase_price_edit')
    purchase_amount = fields.Function(fields.Numeric('Cost amount',
            digits=(16, Eval('currency_digits', 2)),
            depends=['currency_digits']),
        'on_change_with_purchase_amount')
    currency = fields.Function(
        fields.Many2One('currency.currency', 'Currency'),
        'on_change_with_currency')
    currency_digits = fields.Function(fields.Integer('Currency Digits'),
        'get_currency_digits')

    def get_currency_digits(self, name=None):
        if self.company.currency:
            return self.company.currency.digits
        return 2

    @fields.depends('company', '_parent_company.currency')
    def on_change_with_currency(self, name=None):
        if self.company and self.company.currency:
            return self.company.currency.id

    @fields.depends('available')
    def on_change_with_purchase_price_edit(self, name=None):
        return self.available

    @fields.depends('purchase_price', 'company', '_parent_company.currency',
        methods=['_get_cost_quantity'])
    def on_change_with_purchase_amount(self, name=None):
        qty = self._get_cost_quantity()
        if qty and self.purchase_price:
            qty = Decimal(str(qty or '0.0'))
            price = self.purchase_price or Decimal('0.0')
            return self.company.currency.round(price * qty)

    @fields.depends('quantity')
    def _get_cost_quantity(self):
        return self.quantity
